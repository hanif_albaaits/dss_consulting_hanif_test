package testInterview;

import java.util.Arrays;

public class Soal2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		contohnya:
//		firstEnd([1, 2, 3]) → [1, 3]
//		firstEnd([1, 2, 3, 4]) → [1, 4]

		int [] input = new int []{1,3,4,5};
		int [] hasil = new int []{ input[0], input[input.length-1]};
		
		System.out.println("input : "+Arrays.toString(input));
		System.out.println("firstEnd : "+Arrays.toString(hasil));
		
	}

}
