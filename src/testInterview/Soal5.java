package testInterview;

import java.util.ArrayList;
import java.util.List;

public class Soal5 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		contoh: 
		int banyak_pencarian = 28;
		
		for (int i = 1; i <= banyak_pencarian; i++) {
			System.out.print("bilangan "+i+ " = ");
			List<Integer> pembagian = new ArrayList<Integer>();
			for (int j = 1; j < i; j++) {
				if (i % j == 0) {
					pembagian.add(j);
				}
			}
			
			int jumlah = 0;
			for (Integer integer : pembagian) {
				jumlah += integer;
			}
			
			if (jumlah == i) {
				System.out.println(" sempurna");
			} else {
				System.out.println(" tidak sempurna");
			}
		}
		
		
	}

}
