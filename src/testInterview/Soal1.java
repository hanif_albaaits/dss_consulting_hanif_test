package testInterview;

public class Soal1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		1, 2, 3, Friend, 5, 6, Foe, Friend, 9, 10
//		11, Friend, 13, Foe, 15, Friend, 17, 18, 19, Friend
//		Foe, 22, 23, Friend, 25, 26, 27, FriendFoe, 29, 30
//		31, Friend, 33, 34, Foe, Friend, 37, 38, 39, Friend
//		41, Foe, 43, Friend, 45, 46, 47, Friend, Foe, 50
//		51, Friend, 53, 54, 55, FriendFoe, 57, 58, 59, Friend
//		61, 62, Foe, Friend, 65, 66, 67, Friend, 69, Foe
//		71, Friend, 73, 74, 75, Friend, Foe, 78, 79, Friend
//		81, 82, 83, FriendFoe, 85, 86, 87, Friend, 89, 90
//		Foe, Friend, 93, 94, 95, Friend, 97, Foe, 99, Friend

		int jumlah_bil = 100;
		
		for (int i = 1; i <= jumlah_bil; i++) {
			
			if (i % 4 == 0 && i % 7 == 0) {
				System.out.print("FriendFoe");
			} else if(i % 4 == 0) {
				System.out.print("Friend");
			} else if (i % 7 == 0) {
				System.out.print("Foe");
			} else {
				System.out.print(i);
			}
			
			if(i!=100) {
				System.out.print(", ");
			}
		}
	}

}
