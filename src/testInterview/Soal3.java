package testInterview;

import java.util.UUID;

public class Soal3 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		contoh: 
//		random UUID: 18e-2d6bb-4b65-4b07-8e61-cd35d5a18cbc 
//		expected result: 18E2D6

		UUID uuid = UUID.randomUUID();
		String replace = uuid.toString().replaceAll("-", ""); //uuid selalu pake strip
		System.out.println(replace.substring(0, 6).toUpperCase());
	}

}
