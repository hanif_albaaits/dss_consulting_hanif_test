package testInterview;

public class Soal6 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		contoh: 
//		We have two monkeys, a and b, and the parameters aSmile and bSmile indicate 
//		if each is smiling. We are in trouble if they are both smiling or if neither of 
//		them is smiling. Return true if we are in trouble.
//
//		monkeyTrouble(true, true) → true 
//		monkeyTrouble(false, false) → true 
//		monkeyTrouble(true, false) → false
		
//		kondisi nya xnor
//		0 0 = 1
//		0 1 = 0
//		1 0 = 0
//		1 1 = 1
		
		boolean a = monkeyTrouble(true, true);
		boolean b = monkeyTrouble(false, false);
		boolean c = monkeyTrouble(true, false);
		
		System.out.println(a);
		System.out.println(b);
		System.out.println(c);
	}

	private static boolean monkeyTrouble(boolean kondisi1, boolean kondisi2) {
		return kondisi1 == kondisi2; 
	}
}
